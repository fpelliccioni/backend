SELECT *
FROM campaigns AS c
INNER JOIN recipients AS r ON c.campaign_id = r.campaign_id
INNER JOIN users      AS u ON r.user_id = u.user_id
WHERE strftime('%s', 'now') < c.expires
