-- DELETE FROM revocations;
-- DELETE FROM contributionCommitments;
-- DELETE FROM commitments;
-- DELETE FROM contributions;
-- DELETE FROM fullfillments;
-- delete from recipients;
-- delete from users;
-- delete from campaigns;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (1, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (2, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (3, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (4, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (5, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (6, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (7, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (8, 1593043200, 1608854400);
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (9, 1593043200, 1608854400);

-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (1, 'Remove the limitation of 25/50 chained transactions', 'We understand that removing this limitation is desired by a good part of the Bitcoin Cash community. So we...', '', 'bitcoincash:qp7we6jj3u6zdg2mjck2stz5l4fwz3s9jc7w8xprtf');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (2, 'High-performance SLP full indexer', 'Single Ledger Protocol (SLP) has gained momentum in the Bitcoin Cash community as a second layer protocol. We ... ', '', 'bitcoincash:qq77mmau5wwzpls475rgxac5utqn39uvdykd454fmu');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (3, 'High-performance mining API', 'Knuth focuses on performance. One of the areas in which performance becomes a differentiating factor is mining. Our ...', '', 'bitcoincash:qrnsscxdkhn6h8ehjtrs2crn62xdvp6y8yrulcn6f6');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (4, 'Implement Xthinner', 'Block propagation is of great importance to the network, in general, but in particular, for miners and pool operators. Xthinner ...', '', 'bitcoincash:qrq6gpmvs2gk9hwvfjxr6tv2ffr4vjp0552m2k03ws');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (5, 'Implement fast synchronization', 'Having a node ready to be used for mining as quickly as possible is one of our goals. That is why we want to implement UTXO ...', '', 'bitcoincash:qrzcgxhakaqk73p0xupu3mcpmmh4cguvaqtlq5gmj2');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (6, 'Double-SHA256 algorithm optimization', 'The SHA256 (double-SHA256) algorithm is of utmost relevance in a Bitcoin Cash node. We have been exploring how to substantially ...', '', 'bitcoincash:qp2lahla2z2twwn3e5aet7l3wqc3v4a9dgaf73g6ew');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (7, 'Merkle-root algorithm optimization', 'As Bitcoin Cash scale and adoption increase, the ecosystem will see more transactions per block. It is worth mentioning that ...', '', 'bitcoincash:qq220y06nk6vhyujy0h3smx0dkw0xgrhzcrhsel9eg');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (8, 'Improve libraries and languages’ APIs', 'Maintenance and continuous improvement of libraries and APIs is a priority. Although this is continuous work, we would like to ...', '', 'bitcoincash:qqsx35f0gr6j09ty65ktyag5snr22kmetqpn55zrr0');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (9, 'Improve documentation', 'Documentation is a fundamental tool for Knuth''s exposure and proper use. Although this is also a continuous job, we would like ...', '', 'bitcoincash:qq80rj9vrt7wgjtww4gxr5ct88pzuh4f9yfe66fh35');

-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (1,  20000000000, 1,  1);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (2,  24000000000, 2,  2);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (3,  24000000000, 3,  3);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (4,  16000000000, 4,  4);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (5,  24000000000, 5,  5);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (6,  16000000000, 6,  6);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (7,  12000000000, 7,  7);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (8,  24000000000, 8,  8);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (9,  8000000000, 9,  9);



-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 1;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 2;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 3;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 4;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 5;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 6;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 7;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 8;
-- UPDATE campaigns SET expires = 1593043201 WHERE campaign_id = 9;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (10, 1597802400, 1599141600,'Knuth node as a C#/.Net library');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (10, 'Knuth node as a C#/.Net library', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (10,  8000000000, 10,  10);

-- ------------------------------------------------------------------------------------------------
-- -- Knuth node as a C#/.Net library (Finished/Funded)
-- ------------------------------------------------------------------------------------------------
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (10, 1597802400, 1599141600,'Knuth node as a C#/.Net library');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (10, 'Knuth node as a C#/.Net library', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (10,  8000000000, 10,  10);
-- ------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------
-- Knuth node as a Javascript library
------------------------------------------------------------------------------------------------
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (11, 1611576000, 1612180800,'Knuth node as a Javascript library');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (11, 'Knuth node as a Javascript library', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (11,  4800000000, 11,  11);

------------------------------------------------------------------------------------------------

-- UPDATE campaigns SET starts = 1611576000 WHERE campaign_id = 11;
-- UPDATE campaigns SET expires = 1612180800 WHERE campaign_id = 11;

-- UPDATE campaigns SET starts = 1577836801 WHERE campaign_id = 11;
-- UPDATE campaigns SET expires = 1577836802 WHERE campaign_id = 11;

-- UPDATE campaigns SET starts = 1612198800 WHERE campaign_id = 11;
-- UPDATE campaigns SET expires = 1612976400 WHERE campaign_id = 11;


------------------------------------------------------------------------------------------------
-- Knuth Double Spend Proofs implementation
------------------------------------------------------------------------------------------------

-- select * from users;
-- delete from recipients where recipient_id = 12;
-- delete from users where user_id = 12;
-- delete from campaigns where campaign_id = 12;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (12, 1619827200, 1620432000,'Double Spend Proofs implementation');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Double Spend Proofs implementation', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (12,  6400000000, 12,  12);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 12;

-- UPDATE users
-- SET user_url = 'The double-spend-proof is a means with which network participants that have the infrastructure to detect double spends can share that fact so merchants can receive information on their payment app (typically SPV based) in short ...'
-- WHERE user_id = (SELECT MAX(user_id) FROM users);

-- UPDATE recipients
-- SET recipient_satoshis = 4800000000
-- WHERE recipient_id = 12;



-- ------------------------------------------------------------------------------------------------
-- -- Knuth node as a Python library
-- ------------------------------------------------------------------------------------------------
-- -- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (13, 1649851200, 1651752000,'Knuth node as a Python library');
-- -- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (13, 'Knuth node as a Python library', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- -- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (13,  12000000000, 13,  13);


-- select * from users;
-- delete from recipients where recipient_id = 13;
-- --delete from users where user_id = 13;
-- delete from campaigns where campaign_id = 13;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (13, 1649851200, 1651752000,'Knuth node as a Python library');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Knuth node as a Python library', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (13,  12000000000, 13,  13);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 13;

--  UPDATE recipients
--  SET user_id = (SELECT MAX(user_id) FROM users)
--  WHERE recipient_id = 13;


-- UPDATE campaigns
-- SET starts = 1649851200,
-- expires = 1651752000,
-- title = 'Knuth node as a Python library'
-- WHERE campaign_id = 13;


-- ------------------------------------------------------------------------------------------------
-- -- Bringing CashTokens to the Knuth Node
-- -- Supporting the May 2023 Bitcoin Cash Hard Fork

-- ------------------------------------------------------------------------------------------------
-- -- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (14, 1672099200, 1673308800,'Supporting the May 2023 Bitcoin Cash Hard Fork');
-- -- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (14, 'Supporting the May 2023 Bitcoin Cash Hard Fork', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- -- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (14,  12000000000, 14,  14);


-- select * from users;
-- delete from recipients where recipient_id = 14;
-- --delete from users where user_id = 14;
-- delete from campaigns where campaign_id = 14;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (14, 1672099200, 1673308800,'Supporting the May 2023 Bitcoin Cash Hard Fork');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Supporting the May 2023 Bitcoin Cash Hard Fork', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (14,  12000000000, 14,  14);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 14;



-- ------------------------------------------------------------------------------------------------




-- ------------------------------------------------------------------------------------------------
-- -- Propel Knuth to the Next Level: Enhancing APIs and Node Performance!


-- ------------------------------------------------------------------------------------------------
-- -- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (15, 1681732800, 1682942400,'Supporting the May 2023 Bitcoin Cash Hard Fork');
-- -- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES (15, 'Supporting the May 2023 Bitcoin Cash Hard Fork', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- -- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (15,  15000000000, 15,  15);


-- -- select * from users;
-- delete from recipients where recipient_id = 15;
-- --delete from users where user_id = 15;
-- delete from campaigns where campaign_id = 15;

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (15, 1681732800, 1682942400,'Propel Knuth to the Next Level: Enhancing APIs and Node Performance!');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Propel Knuth to the Next Level: Enhancing APIs and Node Performance!', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (15,  15000000000, 15,  15);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 15;


-- ------------------------------------------------------------------------------------------------
-- -- Knuth's Evolution: Powering Bitcoin Cash Directly from Your Browser

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (16, 1693396800, 1694520000, 'Knuth''s Evolution: Powering Bitcoin Cash Directly from Your Browser');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Knuth''s Evolution: Powering Bitcoin Cash Directly from Your Browser', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (16,  10500000000, 16,  16);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 16;

-- ------------------------------------------------------------------------------------------------
-- -- ABLA Integration, Performance with Web Workers and Next-Gen APIs
-- -- 2023-12-07 to 2023-12-21


-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (17, 1701950400, 1703160000, 'ABLA Integration, Performance with Web Workers and Next-Gen APIs');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'ABLA Integration, Performance with Web Workers and Next-Gen APIs', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (17,  9000000000, 17,  17);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 17;

-- UPDATE campaigns
-- SET starts = 1702209600, expires = 1703419200
-- WHERE campaign_id = 17;



------------------------------------------------------------------------------------------------

-- Implementing VM Limits and BigInt CHIPs for the Bitcoin Cash May 2025 Upgrade
-- 2024-11-19 to 2024-12-03

-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires,title) VALUES (18, 1732017600, 1733227200, 'Implementing VM Limits and BigInt CHIPs for the Bitcoin Cash May 2025 Upgrade');
-- INSERT OR IGNORE INTO users (user_id, user_alias, user_url, user_image, user_address) VALUES ((SELECT MAX(user_id) FROM users) + 1, 'Implementing VM Limits and BigInt CHIPs for the Bitcoin Cash May 2025 Upgrade', 'Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem...', '', 'bitcoincash:qpfxhrvqpxqcg58khv6gx857t2xrsk3x8usxfkc72d');
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (18,  9000000000, 18,  18);

-- UPDATE recipients
-- SET user_id = (SELECT MAX(user_id) FROM users)
-- WHERE recipient_id = 18;

-- 120 BCH instead of 90 BCH
UPDATE recipients
SET recipient_satoshis = 12000000000
WHERE recipient_id = 18;


------------------------------------------------------------------------------------------------
















------------------------------------------------------------------------------------------------


-- /**********************************************/
-- /* Add the initial campaign (from 2020-05-18 to 2020-05-21)*/
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (1, 1589760000, 1590062399);

-- /* Add the recipient. */
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (1, 'https://twitter.com/ferpelliccioni', 'http://kth.cash/img/logo.svg', 'Knuth Project', 'bitcoincash:qrmpc65246nmaju9ncxjq7ura9msu5dapgzev398lt');

-- /* Link recipient to the initial campaign. */
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (1, 2000000, 1, 1);


-- /**********************************************/

-- /* Add the initial campaign (from 2020-05-18 to 2020-05-21)*/
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (2, 1589760000, 1590082399);

-- /* Add the recipient. */
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (2, 'https://twitter.com/ferpelliccioni', 'https://pbs.twimg.com/profile_images/1161298208200568833/leZYvsV8_400x400.jpg', 'Knuth Project', 'bitcoincash:qr6wa3qh7gnfk0uryyvms33dhvcrmf3huu7z7c75q9');

-- /* Link recipient to the initial campaign. */
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (2, 3000000, 2, 2);


-- /**********************************************/

-- /* Add the initial campaign (from 2020-05-18 to 2020-05-21)*/
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (3, 1589760000, 1590102399);

-- /* Add the recipient. */
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (3, 'https://twitter.com/ferpelliccioni', 'http://kth.cash/img/logo.svg', 'Knuth Project', 'bitcoincash:qqu2s4dt2hrk4dneywedhtup60uzxs09ms26k58s4v');

-- /* Link recipient to the initial campaign. */
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (3, 4000000, 3, 3);























/**********************************************/

-- /* Add the initial campaign (from 2020-02-01 to 2020-03-21)*/
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (1, 1580508000, 1584748799);

-- /* Add the recipient. */
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (1, 'https://twitter.com/monsterbitar', 'https://pbs.twimg.com/profile_images/490950516973637633/LkJHQeNM_400x400.png', 'Jonathan Silverblood', 'bitcoincash:qr4aadjrpu73d2wxwkxkcrt6gqxgu6a7usxfm96fst');

-- /* Link recipient to the initial campaign. */
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (1, 2000000, 1, 1);

-- /**********************************************/

-- /* Add the initial campaign (from 2020-02-01 to 2020-02-28)*/
-- INSERT OR IGNORE INTO campaigns (campaign_id, starts, expires) VALUES (2, 1584230400, 1585439999);

-- /* Add both EatBCH projects as users. */
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (2, 'https://eatbch.org/venezuela/', 'https://eatbch.org/_assets/img/venezuela-hero-wide.jpg?0.0.11', 'EatBCH Venezuela',  'bitcoincash:ppwk8u8cg8cthr3jg0czzays6hsnysykes9amw07kv');
-- INSERT OR IGNORE INTO users (user_id, user_url, user_image, user_alias, user_address) VALUES (3, 'https://eatbch.org/south-sudan/', 'https://eatbch.org/_assets/img/south-sudan-hero-wide.jpg?0.0.11', 'EatBCH South Sudan', 'bitcoincash:qrsrvtc95gg8rrag7dge3jlnfs4j9pe0ugrmeml950');

-- /* Link both EathBCH projects as recipient to the initial campaign. */
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (2,  700000000, 2, 2);
-- INSERT OR IGNORE INTO recipients (recipient_id, recipient_satoshis, user_id, campaign_id) VALUES (3, 1300000000, 3, 2);
